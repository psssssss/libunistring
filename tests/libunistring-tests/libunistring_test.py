# -*- coding: utf-8 -*-

import os
import subprocess
import logging

""" 
Creating a result.log file for saving logs
"""
logging.basicConfig(level=logging.INFO)

logging.info("TEST RESULTS FOR <uniconv.h> FUNCTION\n\n")

data = []

def create_testfile():
    testfile = subprocess.call("./test 'Bonjour le monde'" , shell=True)
    with open("./testfile.in") as fh:
        lines = fh.readlines()
        global data
        data = lines

def test_utf8():
    """
    Check UTF-8 encoding
    """
    try:
        conv8 = data[0].strip("\n").decode('utf-8')
        conv16 = data[0].strip("\n").decode('utf-16')
        if conv8 == ("Bonjour le monde") and conv16 != ("Bonjour le monde"):
            logging.info("UTF-8 Conversion Test: SUCCESS")
        else:
            logging.error("UTF-8 Conversion Test: ERROR")
    except UnicodeError:
        logging.error("UTF-8 Conversion Test: UnicodeError")
            
def test_utf_16():
    """
    Check UTF-16 encoding
    """
    try:
        conv16 = data[1].strip("\n").decode('utf-16')
        conv8 = data[1].strip("\n").decode('utf-8')
        if conv16 == ("Bonjour le monde") and conv8 != ("Bonjour le monde"):
            logging.info("UTF-16 Conversion Test: SUCCESS")
        else:
            logging.error("UTF-16 Conversion Test: ERROR")
    except UnicodeError:
        logging.error("UTF-16 Conversion Test: UnicodeError")

def test_utf_32():
    """
    Check UTF-32 encoding
    """
    try:
        conv8 = data[2].strip("\n").decode('utf-8')
        conv16= data[2].strip("\n").decode('utf-16')
        conv32 = data[2].strip("\n").decode('utf-32')
        if conv32 == ("Bonjour le monde") and conv8 and conv16 != ("Bonjour le monde"):
            logging.info("UTF-32 Conversion Test: SUCCESS")
        else:
            logging.error("UTF-32 Conversion Test: ERROR")
    except UnicodeError:
        logging.error("UTF-32 Conversion Test: UnicodeError")

def del_testfile():
    try:
        subprocess.call(['rm', '-rf', './testfile.in'])
    except OSError as e:
        logging.error("OSError\n")


if __name__ == "__main__":
    del_testfile()
    create_testfile()
    test_utf8()
    test_utf_16()
    test_utf_32()

